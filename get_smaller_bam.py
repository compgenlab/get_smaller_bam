#!/usr/bin/env python

import argparse
import os
import sys
import random

def getVariants(vcf, high, moderate):
    ranges = {}
    highVariants = []
    moderatesVariants = []
    with open(vcf) as f:
        for line in f:
            if not line.startswith("#"):
                infoField = line.split()[7]
                if "|HIGH|" in infoField:
                    chr = line.split()[0]
                    value = int(line.split()[1])
                    highVariants.append(chr + "_" + str(value) )
                if "|MODERATE|" in infoField:
                    chr = line.split()[0]
                    value = int(line.split()[1])
                    moderatesVariants.append(chr + "_" + str(value) )

    highVariants = random.sample(highVariants, high)
    moderatesVariants = random.sample(moderatesVariants, moderate)
    for variant in highVariants:
        chr,pos = variant.split("_")
        ranges.setdefault(chr, [])
        ranges.get(chr).append(int(pos))

    for variant in moderatesVariants:
        chr,pos = variant.split("_")
        ranges.setdefault(chr, [])
        ranges.get(chr).append(int(pos))

    return ranges


def fromPositionToRange(positions, range = 1000):
    d = {}
    for key in positions.keys():
        listOfPositions = positions.get(key)
        ranges = [(el-range, el+range) for el in listOfPositions]
        d[key] = ranges
    return d


def collapaseRanges(ranges):
    d = {}
    for key in ranges:
        a = ranges.get(key)
        b = []
        for begin,end in sorted(a):
            if b and b[-1][1] >= begin - 1:
                b[-1][1] = max(b[-1][1], end)
            else:
                b.append([begin, end])
        d[key] = b
    return d


def fromRangesToString(ranges):
    coordinates = []
    for key in collapsedRanges.keys():
        for positions in collapsedRanges.get(key):
            coordinates.append(key + ":" + str(positions[0]) + "-" + str(positions[1]) )
    return coordinates


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Producing a smaller BAM by choosing variants from an annotated VCF")
    parser.add_argument('-v', '--vcf', action='store', type=str, help="Variant Caller file from which get variants", required=True)
    parser.add_argument('-u', '--high', action='store', type=int, help="Number of HIGH effect variants (default: 1)", default=1)
    parser.add_argument('-m', '--moderate', action='store', type=int, help="Number of MODERATE effect variants (default: 1)", default=1)
    parser.add_argument('-r', '--range', action='store', type=int, help="Nucleotides to be added left and right (default: 100000)", default=100000)
    parser.add_argument('-b', '--bam', action='store', type=str, help="Big bam file to be reduced", required=True)
    parser.add_argument('-n', '--bam2', action='store', type=str, help="Second bam to be reduced using same positions found in bam passed with --bam. Usaully the normal version of a tumoral bam", required=False)
    parser.add_argument('-o', '--output', action='store', type=str, help="Name of output bam (default: smaller_bam.bam)", required=False, default="smaller_bam.bam")

    args = parser.parse_args()

    variants =  getVariants(args.vcf, args.high, args.moderate)
    ranges = fromPositionToRange(variants, range = args.range)
    collapsedRanges = collapaseRanges(ranges)
    coordinates = fromRangesToString(collapsedRanges)
  
    command = "samtools view -b " + args.bam + " " + " ".join(coordinates) + " > " + args.output
    print command
    os.system(command)
    
    command = "samtools sort " +  args.output + " " + " -o sorted_" + args.output[:-4] + ".bam"
    print command
    os.system(command)

    command = "samtools index " + "sorted_" + args.output[:-4] + ".bam"
    print command
    os.system(command)

    if args.bam2:
        command = "samtools view -b " + args.bam2 + " " + " ".join(coordinates) + " > " + "normal_" + args.output
        print command
        os.system(command)
    
        command = "samtools sort " +  "normal_" + args.output + " " + " -o sorted_normal_" + args.output[:-4] + ".bam"
        print command
        os.system(command)

        command = "samtools index " + "sorted_normal_" + args.output[:-4] + ".bam"
        print command
        os.system(command)


