# Getting smaller annotated .bam files

Creating a smaller _bam_ from an already annotated "big" one. The _bam_ is created by choosing the number of HIGH and MODERATE variants we want in it.


### List of parameters:

| Extended parameter        | Short parameter | Default Variable| Type | What is it? |
| ------------- |:-------------| :-------------| :-------------| :-------------| 
| --vcf      | -v | None | String | The file with variants in _vcf_ format |
| --bam | -b | None | String |  Big bam file to be reduced |
| --bam2 | -n | None | String |  Second bam to be reduced using same positions found in bam passed with --bam. Usaully the normal version of a tumoral bam. Optional argument |
| --high | -u | 1 | Number |  Number of HIGH effect variants (default: 1) |
| --moderate | -m | 1 | Number | "Number of MODERATE effect variants (default: 1) |
| --range | -r | 100000 | Number | Nucleotides to be added left and right (default: 100000) |
| --output | -o | smaller_bam.bam | String | Name of output bams |


### Examples:

One bam example:

```bash
[rambo@pc]./get_smaller_bam.py -v variants.vcf -u 1 -m 1 -b am_rem.bam
samtools view -b am_rem.bam 1:3603819-3803819 5:10888331-11088331 > smaller_bam.bam
samtools sort smaller_bam.bam -o sorted_smaller_bam
samtools index sorted_smaller_bam.bam
```

Two bam example:

```bash
[rambo@pc]./get_smaller_bam.py -v variants.vcf -u 1 -m 1 -b am_ric.bam -n am_rem.bam
samtools view -b am_ric.bam 12:25298284-25498284 21:10949623-11149623 > smaller_bam.bam
samtools sort smaller_bam.bam  -o sorted_smaller_bam.bam
samtools index sorted_smaller_bam.bam
samtools view -b am_rem.bam 12:25298284-25498284 21:10949623-11149623 > normal_smaller_bam.bam
samtools sort normal_smaller_bam.bam  -o sorted_normal_smaller_bam.bam
samtools index sorted_normal_smaller_bam.bam
```
